#!/bin/bash
## script de copie rsync
## Usage: ./sauvegarde.sh
##  -s <dossier à sauvegarder>
##  -d <destination de la sauvegarde>
##  -m [OPTIONNEL] monte/démonte le dossier de destination

args="$*"
scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dateYmdhms=$(date +"%Y-%m-%d %H:%M:%S")
dateYmd=$(date +"%Y-%m-%d")
dateYm=$(date +"%Y-%m")
exitMsg="Appuyez sur une touche pour quitter"

main()
{
    ##traitement des options
    get_options "$@"

    ## si les options obligatoires ne sont pas définies affichage des informations d'utilisation
    if [ -z "${source}" ] || [ -z "${destination}" ] ; then
        usage
    fi

    ## monte le dossier de destination si 
    ##  - le script est lancé avec l'option -m
    ##  - le dossier n'est pas déjà monté
    ism=$(is_mounted "${destination}") 
    if [ "${mount}" == true ] && [ "${ism}" -eq 1 ] ; then
        wrap_exec mount "${destination}"   
        wait     
    fi
    ## vérifications
    if [ ! -d "$destination" ] ; then
        log "Le dossier ""$destination"" n'existe pas"
        read -n1 -p $'\n'"$exitMsg"$'\n'
        exit 1
    fi
    if [ ! -w "$destination" ] ; then
        log "Le dossier ""$destination"" n'est pas accessible en écriture"
        read -n1 -p $'\n'"$exitMsg"$'\n'
        exit 1
    fi

    ## synchronisation
    wrap_exec rsync -aq --delete-after --log-file=${scriptDir}/${dateYmd}-rsync.log ${source} ${destination}
    wait

    ## démonte le dossier de destination si il y a l'option -m et que le dossier est monté
    ism=$(is_mounted "${destination}") 
    if [ "${mount}" == true ] && [ "${ism}" -eq 0 ] ; then
        wrap_exec umount "${destination}"
    fi
    
    ## effacement des anciens fichiers logs
    wrap_exec find "$scriptDir" -type f -mtime +7 -name '*rsync.log' -execdir rm -- '{}' \;
    wrap_exec find "$scriptDir" -type f -mtime +90 -name '*error.log' -execdir rm -- '{}' \;

    exit 0
}

## informations sur l'utilisation
usage()
{
    printf "Usage: %s\n  -s <dossier à sauvegarder>\n  -d <destination de la sauvegarde>\n  -m [OPTIONNEL] monte/démonte le dossier de destination\n" "$0" 1>&2;
    read -n1 -p $'\n'"$exitMsg"$'\n'
    exit 0;
}

## affiche le message sur stderr et l'ajoute au fichier de log
log()
{
    echo "${dateYmdhms} \"$args\" : $*" 2>&1 | tee -a "${scriptDir}/${dateYm}"-error.log
}

## parcours des options
get_options()
{
    local OPTIND
    while getopts ":s:d:m" o; do
        case "${o}" in
            s)
                source=${OPTARG}
                if [ ! -d "$source" ] ; then
                    log "Le dossier ""$source"" n'existe pas"
                    read -n1 -p $'\n'"$exitMsg"$'\n'
                    exit 1
                fi
                if [ ! -r "$source" ] ; then
                    log "Le dossier ""$source"" n'est pas lisible"
                    read -n1 -p $'\n'"$exitMsg"$'\n'
                    exit 1
                fi
                if [ -z "$(ls -A "$source")" ]; then
                    log "Le dossier ""$source"" est vide"
                    read -n1 -p $'\n'"$exitMsg"$'\n'
                    exit 1
                fi
                ;;
            d)
                destination=${OPTARG}
                ;; 
            m)
                mount=true
                ;;
            *)
                usage
                ;;
        esac
    done
    shift "$((OPTIND-1))"
}

## est-ce que le dossier est monté ?
is_mounted()
{
    dir=$1
    if [[ $(findmnt -M "$dir") ]]; then
        echo 0
    else
        echo 1
    fi
}

## wrapper pour les commandes
## log les messages d'erreur
wrap_exec()
{
    command="$*"
    error=$($command 2>&1 1>/dev/null)
    if [ $? -ne 0 ]; then
        log "'$command' ${error}"
        read -n1 -p $'\n'"$exitMsg"$'\n'
        exit 1
    fi
}

## execution de la fonction principale
main "$@"
