# Script shell de sauvegarde rsync

## Installation

- rendre le script éxécutable (ex: chmod +x)
- renommer le fichier sauvegarde.desktop-ubuntu en sauvegarde.desktop puis personnaliser les paramètres

## Utilisation

### Options

- -s dossier à sauvegarder
- -d destination de la sauvegarde
- -m [OPTIONNEL] monte/démonte le dossier de destination

### Exemple

```console
foo@bar:~$ ./sauvegarde.sh -s /home/Documents - d /media/mount -m
```

## Crédits

icone : <https://freeicons.io/app-icons/file-document-cloud-folder-icon-50186>

Licence Creative Commons(Attribution-NonCommercial 3.0 unported)
